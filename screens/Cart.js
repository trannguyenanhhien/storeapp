import React from "react";
import {
  StyleSheet,
  FlatList,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { selectCart, removeToCart, addToCart } from "../feature/cartSlice";
import CartListItem from "../components/CartListItem";
import { auth } from "../firebase";
var { height, width } = Dimensions.get("window");
export default function Cart(props) {
  const { navigation } = props;
  const food = useSelector(selectCart);
  const dispatch = useDispatch();
  const onRemoveToCartClick = (item) => {
    dispatch(removeToCart(item));
  };
  const onAddToCartClick = (item) => {
    dispatch(addToCart(item));
  };
  var sum = 0;
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Cart Food</Text>
      </View>
      <FlatList
        data={food}
        numColumns={1}
        renderItem={({ item }) => (
          <View style={styles.wrapper}>
            <CartListItem
              food={item}
              onAddToCartClick={() => onAddToCartClick(item)}
              onRemoveToCartClick={() => onRemoveToCartClick(item)}
            />
          </View>
        )}
        keyExtractor={(item) => `${item.id}`}
      />
      <View style={{ height: height / 70 }}></View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          marginBottom: 5,
        }}
      >
        <Text style={styles.total}>Total:</Text>
        <View style={{ width: width / 2 }}></View>
        <Text style={[styles.total, { color: "black" }]}>
          {food.forEach((item) => {
            sum = sum + item.count * item.price;
          })}
          ${sum}
        </Text>
      </View>
      <View style={{}}>
        <TouchableOpacity
          style={styles.touchCheckout}
          onPress={() => {
            if (auth.currentUser?.email) {
              if (food.length != 0) {
                navigation.navigate("Checkout", {
                  totalPrice: sum,
                  food: food,
                });
              } else {
                alert("There's nothing in cart");
              }
            } else {
              navigation.navigate("Login");
              alert("You have to log in before");
            }
          }}
        >
          <Text style={styles.textCheckout}>CHECKOUT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingTop: 16,
    flex: 1,
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: 8,
  },
  touchCheckout: {
    backgroundColor: "red",
    width: width - 40,
    alignItems: "center",
    padding: 10,
    borderRadius: 5,
    marginLeft: 12,
    marginBottom: 10,
  },
  textCheckout: {
    fontSize: 24,
    fontWeight: "bold",
    color: "white",
  },
  title: {
    fontSize: 28,
    color: "gray",
    marginTop: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  total: {
    color: "gray",
    fontSize: 27,
    fontWeight: "bold",
    letterSpacing: 1,
    fontStyle: "italic",
  },
});
