import React from "react";
import {
  StyleSheet,
  FlatList,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import SAItem from "../../components/SAItem";
var { height, width } = Dimensions.get("window");
export default function ShippingAddresses(props) {
  const listInfo = [
    {
      name: "Jane Doe1",
      address: "3 Newbridge Court",
      state: "Chino Hills, CA 91709, U.S",
      edit: true,
    },
    {
      name: "Jane Doe2",
      address: "3 Newbridge Court",
      state: "Chino Hills, CA 91709, U.S",
      edit: true,
    },
    {
      name: "Jane Doe3",
      address: "3 Newbridge Court",
      state: "Chino Hills, CA 91709, U.S",
      edit: true,
    },
  ];
  return (
    <View style={{ flex: 1, padding: width / 25, width: width }}>
      <FlatList
        data={listInfo}
        renderItem={({ item }) => (
          <View>
            <SAItem info={item} />
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
      <View style={{ alignItems: "center" }}>
        <MaterialCommunityIcons
          name="plus-box-multiple"
          color={"black"}
          size={30}
        />
      </View>
    </View>
  );
}
