import React, { useState } from "react";
import {
  StyleSheet,
  Image,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
  Alert,
} from "react-native";
import SAItem from "../../components/SAItem";
import { useDispatch } from "react-redux";
import { addToOrder } from "../../feature/orderSlice";
import { deleteToCart } from "../../feature/cartSlice";
var { height, width } = Dimensions.get("window");
var date = new Date();
export default function Checkout(props) {
  const dispatch = useDispatch();
  const onAddToOrderClick = (item) => {
    dispatch(addToOrder(item));
  };
  const { route, navigation } = props;
  const { totalPrice, food } = route.params;

  const [percent, setPercent] = useState(0.2);
  var total = parseFloat(
    parseFloat(JSON.stringify(totalPrice) * `${percent}`) +
      parseFloat(JSON.stringify(totalPrice))
  ).toFixed(2);
  const [test, setTest] = useState({
    id1: true,
  });
  const info = {
    id: 1,
    name: "Jane Doe",
    address: "3 Newbridge Court",
    state: "Chino Hills, CA 91709, U.S",
  };

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 3 }}>
          Shipping address
        </Text>
        <TouchableOpacity
          style={{ alignSelf: "flex-end", marginVertical: width / 20 - 10 }}
          onPress={() => {
            navigation.navigate("ShippingAddresses");
          }}
        >
          <Text style={[styles.change, { paddingRight: width / 20 }]}>
            Change
          </Text>
        </TouchableOpacity>
      </View>
      <SAItem info={info} />
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginTop: height / 20,
        }}
      >
        <Text style={{ fontSize: 20, fontWeight: "bold" }}>Payment</Text>
        <TouchableOpacity
          style={{ alignSelf: "flex-end", marginVertical: width / 20 - 10 }}
        >
          <Text style={[styles.change, { paddingRight: width / 20 }]}>
            Change
          </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginBottom: height / 20,
        }}
      >
        <View
          style={{
            borderRadius: 12,
            shadowOpacity: 0.3,
            shadowRadius: 3,
            shadowOffset: {
              height: 0,
              width: 0,
            },
            elevation: 3,
          }}
        >
          <Image
            source={{
              uri: "https://files.123inventatuweb.com/ce/9d/ce9d55a9-022a-4437-b9ba-61ca60c1ca84.jpg",
            }}
            style={{
              width: width / 5,
              height: width / 8,
              borderRadius: 12,
            }}
          />
        </View>
        <Text style={{ fontSize: 17, paddingHorizontal: 20 }}>
          **** **** **** 3947
        </Text>
      </View>

      <View>
        <Text
          style={{
            fontSize: 20,
            fontWeight: "bold",
            marginBottom: width / 20,
          }}
        >
          Delivery method
        </Text>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              setTest({
                id1: true,
                id2: false,
                id3: false,
              });
              setPercent(0.2);
            }}
          >
            <View
              style={[
                styles.boxDeli,
                {
                  borderWidth: test.id1 ? 2 : 0,
                  borderColor: test.id1 ? "red" : null,
                },
              ]}
            >
              <Image
                source={{
                  uri: "https://donhang.ghn.vn/static/media/Giao_Hang_Nhanh_Toan_Quoc_color.b7d18fe5.png",
                }}
                style={styles.imgDeli}
              />
              <Text style={{ color: "gray" }}>2-3 days</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setTest({ id2: true, id1: false, id3: false });
              setPercent(0.25);
            }}
          >
            <View
              style={[
                styles.boxDeli,
                {
                  borderWidth: test.id2 ? 2 : 0,
                  borderColor: test.id2 ? "red" : null,
                },
              ]}
            >
              <Image
                source={{
                  uri: "https://newstarpaper.vn/wp-content/uploads/2021/03/logo-ghtk.png",
                }}
                style={styles.imgDeli}
              />
              <Text style={{ color: "gray" }}>2-3 days</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setTest({ id3: true, id1: false, id2: false });
              setPercent(0.3);
            }}
          >
            <View
              style={[
                styles.boxDeli,
                {
                  borderWidth: test.id3 ? 2 : 0,
                  borderColor: test.id3 ? "red" : null,
                },
              ]}
            >
              <Image
                source={{
                  uri: "https://www.ninjavan.co/static/26d77dcfca18bf342e97d216a3cdd5e0/8f2d5/placeholding-banner-logo_optimized.png",
                }}
                style={styles.imgDeli}
              />
              <Text style={{ color: "gray" }}>1-3 days</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>

      <View
        style={{ flex: 1, justifyContent: "flex-end", alignItems: "center" }}
      >
        <View style={{ flexDirection: "row", marginBottom: width / 20 }}>
          <Text style={{ flex: 1, fontSize: 18, color: "gray" }}>Order:</Text>
          <Text style={{ alignItems: "flex-end", fontSize: 18 }}>
            ${parseFloat(JSON.stringify(totalPrice))}
          </Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: width / 20 }}>
          <Text style={{ flex: 1, fontSize: 18, color: "gray" }}>
            Delivery:
          </Text>
          <Text style={{ alignItems: "flex-end", fontSize: 18 }}>
            ${parseFloat(JSON.stringify(totalPrice) * `${percent}`).toFixed(2)}
          </Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: width / 20 }}>
          <Text
            style={{
              flex: 1,
              fontSize: 22,
              fontWeight: "bold",
              color: "gray",
            }}
          >
            Summary:
          </Text>
          <Text
            style={{
              alignItems: "flex-end",
              fontSize: 22,
              fontWeight: "bold",
            }}
          >
            ${total}
          </Text>
        </View>
        <TouchableOpacity
          style={styles.submit}
          onPress={() => {
            let item = {
              name: info.name,
              address: info.address,
              state: info.state,
              payMeth: "COD",
              deliMeth:
                test.id1 == true
                  ? "GHN"
                  : test.id2 == true
                  ? "GHTK"
                  : "Ninja Van",
              sum: parseFloat(
                parseFloat(JSON.stringify(totalPrice) * `${percent}`) +
                  parseFloat(JSON.stringify(totalPrice))
              ).toFixed(2),
              quantity: food.length,
              status: "Delivered",
              foodIn4: food,
              date: `${date.getDate()}-${
                date.getMonth() + 1
              }-${date.getFullYear()}`,
            };
            Alert.alert(
              "Submit Order",
              "Are you sure?",
              [
                {
                  text: "Cancel",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel",
                },
                {
                  text: "OK",
                  onPress: () => {
                    onAddToOrderClick(item);
                    dispatch(deleteToCart());
                    navigation.navigate("Home");
                    Alert.alert("Success", "Your order is delivered");
                  },
                },
              ],
              { cancelable: false }
            );
          }}
        >
          <Text style={styles.textSub}>SUBMIT ORDER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: width / 25,
    width: width,
  },
  boxDeli: {
    width: width / 4,
    height: width / 5,
    backgroundColor: "white",
    alignItems: "center",
    paddingVertical: width / 20 - 10,
    borderRadius: 12,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    elevation: 3,
  },
  imgDeli: {
    width: width / 5,
    height: width / 12,
    marginBottom: 5,
  },
  change: {
    fontWeight: "bold",
    fontSize: 18,
    color: "red",
  },
  submit: {
    backgroundColor: "red",
    width: width - 40,
    alignItems: "center",
    padding: 10,
    borderRadius: 20,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    elevation: 3,
    margin: width / 20 - 15,
  },
  textSub: {
    fontSize: 20,
    color: "white",
  },
});
