import React, { useState, useEffect } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import ProfileItem from "../components/ProfileItem";
import { auth } from "../firebase";
export default function Settings(props) {
  const { navigation } = props;

  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => {
        navigation.replace("Cart");
      })
      .catch((err) => alert(err.message));
  };
  var index = auth.currentUser?.email.indexOf("@");
  return (
    <View style={{ margin: 20, marginVertical: 50 }}>
      <Text style={{ fontWeight: "bold", fontSize: 34, marginBottom: 20 }}>
        My Profile
      </Text>
      <View style={{ flexDirection: "row", marginBottom: 30 }}>
        <Image
          style={{ width: 100, height: 100, borderRadius: 50 }}
          source={{
            uri: "https://12guns.vn/code-tim-kiem-theo-ten-trong-java/imager_3161.jpg",
          }}
        />
        <View style={{ justifyContent: "center", marginHorizontal: 20 }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: "bold",
              textTransform: "capitalize",
            }}
          >
            {auth.currentUser?.email
              ? auth.currentUser?.email.substr(0, index)
              : "Your name"}
          </Text>
          <Text style={{ color: "gray", fontStyle: "italic" }}>
            {auth.currentUser?.email
              ? auth.currentUser?.email
              : "example@gmail.com"}
          </Text>
        </View>
      </View>
      <View>
        <ProfileItem
          onClick={() => navigation.navigate("Account")}
          info={{ name: "My account", decr: "Security" }}
        />
        <ProfileItem
          onClick={() => navigation.navigate("ShippingAddresses")}
          info={{ name: "Shipping addresses", decr: "3 addresses" }}
        />
        <ProfileItem
          info={{ name: "Payment methods", decr: "Mastercard **47" }}
        />
        <ProfileItem
          info={{ name: "Promocodes", decr: "You have special promocodes" }}
        />
        <ProfileItem
          info={{ name: "My reviews", decr: "Reviews for 4 items" }}
        />
        <ProfileItem
          onClick={() => navigation.navigate("Settings")}
          info={{ name: "Settings", decr: "Notification" }}
        />
        {auth.currentUser?.email ? (
          <View style={styles.container}>
            <TouchableOpacity
              style={[styles.btn, { backgroundColor: "red" }]}
              onPress={handleSignOut}
            >
              <Text style={styles.btnText}>Logout</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.container}>
            <TouchableOpacity
              style={[styles.btn, { backgroundColor: "green" }]}
              onPress={() => navigation.navigate("Login")}
            >
              <Text style={styles.btnText}>Login</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  btn: {
    width: "40%",
    padding: 10,
    borderRadius: 10,
    alignItems: "center",
    marginTop: 40,
  },
  btnText: {
    color: "white",
    fontWeight: "700",
    fontSize: 16,
  },
});
