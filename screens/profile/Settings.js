import React, { useState } from "react";
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
  Switch,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default function Settings(props) {
  const [isEnabled1, setIsEnabled1] = useState(false);
  const [isEnabled2, setIsEnabled2] = useState(false);
  const [isEnabled3, setIsEnabled3] = useState(false);
  const toggleSwitch1 = () => setIsEnabled1((previousState) => !previousState);
  const toggleSwitch2 = () => setIsEnabled2((previousState) => !previousState);
  const toggleSwitch3 = () => setIsEnabled3((previousState) => !previousState);
  const [isEnabled, setIsEnabled] = useState({
    id1: true,
  });
  return (
    <View style={{ margin: 20, marginVertical: 50 }}>
      <Text style={{ fontWeight: "bold", fontSize: 34, marginBottom: 20 }}>
        Settings
      </Text>
      <Text style={{ fontSize: 18, fontWeight: "bold", marginVertical: 10 }}>
        Themes
      </Text>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text style={{ fontSize: 16 }}>Normal</Text>
          <Switch
            trackColor={{ false: "#CFCFCF", true: "#CFCFCF" }}
            thumbColor={isEnabled.id1 ? "#00BB00" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={() => setIsEnabled({ id1: true })}
            value={isEnabled.id1}
          />
        </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text style={{ fontSize: 16 }}>Light</Text>
          <Switch
            trackColor={{ false: "#CFCFCF", true: "#CFCFCF" }}
            thumbColor={isEnabled.id2 ? "#00BB00" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={() => setIsEnabled({ id2: true })}
            value={isEnabled.id2}
          />
        </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text style={{ fontSize: 16 }}>Dark</Text>
          <Switch
            trackColor={{ false: "#CFCFCF", true: "#CFCFCF" }}
            thumbColor={isEnabled.id3 ? "#00BB00" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={() => setIsEnabled({ id3: true })}
            value={isEnabled.id3}
          />
        </View>
      </View>
      <Text style={{ fontSize: 18, fontWeight: "bold", marginVertical: 10 }}>
        Notifications
      </Text>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Text style={{ fontSize: 16 }}>Sales</Text>
        <Switch
          trackColor={{ false: "#CFCFCF", true: "#CFCFCF" }}
          thumbColor={isEnabled1 ? "#00BB00" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch1}
          value={isEnabled1}
        />
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Text style={{ fontSize: 16 }}>New arrivals</Text>
        <Switch
          trackColor={{ false: "#CFCFCF", true: "#CFCFCF" }}
          thumbColor={isEnabled2 ? "#00BB00" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch2}
          value={isEnabled2}
        />
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Text style={{ fontSize: 16 }}>Delivey status changes</Text>
        <Switch
          trackColor={{ false: "#CFCFCF", true: "#CFCFCF" }}
          thumbColor={isEnabled3 ? "#00BB00" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch3}
          value={isEnabled3}
        />
      </View>
    </View>
  );
}
