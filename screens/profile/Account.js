import React, { useState } from "react";
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Modal,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
var { height, width } = Dimensions.get("window");
export default function Account(props) {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View
      style={{
        backgroundColor: modalVisible ? "gray" : "transparent",
        height: height,
      }}
    >
      <View style={{ margin: 20, marginVertical: 50 }}>
        <Modal animationType="slide" transparent={true} visible={modalVisible}>
          <View
            style={{
              backgroundColor: "#F5F5F5",
              height: height,
              marginTop: height / 2.5,
              paddingHorizontal: 20,
              paddingTop: 10,
              borderTopEndRadius: 30,
              borderTopLeftRadius: 30,
            }}
          >
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text></Text>
              <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
                <MaterialCommunityIcons
                  name="chevron-down"
                  size={30}
                  color={"gray"}
                  style={{
                    borderRadius: 15,
                    height: 30,
                    width: 30,
                  }}
                />
              </TouchableOpacity>
            </View>
            <Text
              style={{ fontSize: 20, fontWeight: "bold", textAlign: "center" }}
            >
              Password change
            </Text>
            <TextInput style={styles.input} placeholder="Old Password" />
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text></Text>
              <TouchableOpacity>
                <Text style={{ color: "gray" }}>Forgot Password?</Text>
              </TouchableOpacity>
            </View>
            <TextInput style={styles.input} placeholder="New Password" />
            <TextInput style={styles.input} placeholder="Comfirm Password" />
            <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
              <View
                style={[
                  styles.card,
                  { backgroundColor: "red", borderColor: "red" },
                ]}
              >
                <Text style={{ color: "white", fontSize: 16 }}>
                  Save Password
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>

        <Text style={{ fontWeight: "bold", fontSize: 34, marginBottom: 20 }}>
          My Account
        </Text>
        <Text style={{ fontSize: 18, fontWeight: "bold", marginVertical: 10 }}>
          Personal information
        </Text>
        <TextInput style={styles.input} placeholder="Full name" />
        <TextInput
          style={styles.input}
          placeholder="Email"
          keyboardType="email-address"
        />
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text
            style={{ fontSize: 18, fontWeight: "bold", marginVertical: 10 }}
          >
            Password
          </Text>
          <TouchableOpacity onPress={() => setModalVisible(true)}>
            <Text style={{ fontSize: 16, marginVertical: 10, color: "gray" }}>
              Change
            </Text>
          </TouchableOpacity>
        </View>
        <TextInput style={styles.input} value="**********" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 80,
    padding: 20,
    elevation: 0.5,
    fontSize: 17,
    marginVertical: 10,
  },
  card: {
    borderWidth: 1,
    borderRadius: 20,
    width: width / 1.1,
    alignItems: "center",
    height: width / 8,
    justifyContent: "center",
    marginTop: 10,
  },
});
