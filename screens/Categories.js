import axios from "axios";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Dimensions,
  Image,
  Text,
  TextInput,
} from "react-native";
import { selectAPI } from "../feature/getAPI";
import { Searching, SearchActive } from "../components/Searching";
import { SwiperFlatList } from "react-native-swiper-flatlist";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../feature/cartSlice";
import CategoryListItem from "../components/CategoryListItem";
import FoodListItem from "../components/FoodListItem";
import SearchItem from "../components/SearchItem";
var { height, width } = Dimensions.get("window");

export default function Categories() {
  const api = useSelector(selectAPI);
  const banner = api.banner;
  const categories = api.categories;
  const food = api.food;
  const [catg, setCatg] = useState({
    id: 0,
    name: "Categories",
  });
  const [searchActive, setSearchActive] = useState(false);
  const [filterData, setFilterData] = useState([]);
  const dispatch = useDispatch();
  const onAddToCartClick = (item) => {
    dispatch(addToCart(item));
  };
  const FilterSearch = (text) => {
    const newFood = api.food;
    if (text) {
      const newData = newFood.filter((item) => {
        const textData = text ? text.toUpperCase() : null;
        return item.name.toUpperCase().indexOf(textData) > -1;
      });
      setFilterData(newData);
    } else {
      setFilterData(newFood);
    }
  };
  return (
    <>
      <View style={styles.category}>
        <View
          style={{
            width: width,
            alignItems: "center",
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          {!searchActive ? (
            <>
              <View>
                <Text style={{ color: "transparent" }}>ABC</Text>
              </View>
              <Image
                resizeMode="contain"
                style={[styles.logo]}
                source={{
                  uri: "https://www.tutofox.com/foodapp/foodapp.png",
                }}
              />
              <View>
                <Searching
                  searchClick={() => {
                    setSearchActive(true);
                  }}
                />
              </View>
            </>
          ) : (
            <>
              <FlatList
                ListHeaderComponent={
                  <SearchActive
                    cancelClick={() => {
                      setSearchActive(false);
                    }}
                    searchChange={(text) => FilterSearch(text)}
                  />
                }
                style={{ height: height - 10 }}
                data={filterData}
                renderItem={({ item }) => (
                  <SearchItem
                    item={item}
                    onClick={() => onAddToCartClick(item)}
                  />
                )}
                keyExtractor={(item, index) => index.toString()}
              />
            </>
          )}
        </View>

        <FlatList
          ListHeaderComponent={
            <>
              <View style={styles.container}>
                <SwiperFlatList
                  style={styles.swiper}
                  autoplay
                  autoplayDelay={2}
                  autoplayLoop
                  data={banner}
                  renderItem={({ item }) => (
                    <Image
                      key={item}
                      style={styles.img}
                      source={{ uri: item }}
                      resizeMode="contain"
                    />
                  )}
                />
              </View>
              <Text style={styles.titleCategory}>{catg.name}</Text>
              <View>
                <FlatList
                  horizontal={true}
                  data={categories}
                  renderItem={({ item }) => (
                    <CategoryListItem
                      category={item}
                      onPress={() => {
                        setCatg({
                          id: item.id,
                          name: item.name,
                        });
                      }}
                    />
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </>
          }
          numColumns={2}
          data={food}
          renderItem={({ item }) => {
            if (catg.id == 0 || catg.id == item.categorie) {
              return (
                <FoodListItem
                  food={item}
                  onAddToCartClick={() => onAddToCartClick(item)}
                />
              );
            }
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 8,
    width: width,
    alignItems: "center",
  },
  swiper: {
    height: width / 2,
  },
  img: {
    width: width - 40,
    height: width / 2,
    borderRadius: 10,
    marginHorizontal: 20,
  },
  category: {
    width: width,
    borderRadius: 20,
    flex: 1,
  },
  titleCategory: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 10,
  },
  logo: {
    height: 60,
    width: width / 2,
    marginTop: 30,
  },
});
