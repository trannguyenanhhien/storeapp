import axios from "axios";
import React, { useState, useEffect } from "react";
import { View, Text, FlatList } from "react-native";
import OrderItem from "../../components/OrderItem";

export default function Processing(props) {
  const listInfo = [
    {
      id: 1,
      numOrder: "No123",
      date: "05-02-2022",
      trackNum: "IW2340958",
      quantity: 3,
      amount: 112,
      status: "Processing",
    },
    {
      id: 1,
      numOrder: "No123",
      date: "05-02-2022",
      trackNum: "IW2340958",
      quantity: 3,
      amount: 112,
      status: "Processing",
    },
    {
      id: 1,
      numOrder: "No123",
      date: "05-02-2022",
      trackNum: "IW2340958",
      quantity: 3,
      amount: 112,
      status: "Processing",
    },
    {
      id: 1,
      numOrder: "No123",
      date: "05-02-2022",
      trackNum: "IW2340958",
      quantity: 3,
      amount: 112,
      status: "Processing",
    },
  ];
  return (
    <View>
      <FlatList
        data={listInfo}
        renderItem={({ item }) => <OrderItem info={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}
