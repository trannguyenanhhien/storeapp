import axios from "axios";
import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Dimensions,
  StyleSheet,
} from "react-native";
import OrderDetail from "../../components/OrderDetail";
import { useSelector } from "react-redux";
import { selectOrder } from "../../feature/orderSlice";
var { height, width } = Dimensions.get("window");
export default function OrderDetails(props) {
  const order = useSelector(selectOrder);
  const { route } = props;
  const { item } = route.params;
  return (
    <View style={{ margin: 20 }}>
      <FlatList
        ListHeaderComponent={
          <View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 10,
              }}
            >
              <View>
                <Text
                  style={{ fontSize: 18, fontWeight: "bold", marginBottom: 10 }}
                >
                  Order No{item.id}
                </Text>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "gray", fontSize: 16 }}>
                    Tracking number:{" "}
                  </Text>
                  <Text style={{ fontSize: 16 }}>IW{12584 + item.id}</Text>
                </View>
              </View>
              <View>
                <Text style={{ color: "gray", marginBottom: 10, fontSize: 16 }}>
                  {item.date}
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    color:
                      item.status == "Delivered"
                        ? "green"
                        : item.status == "Processing"
                        ? "orange"
                        : "red",
                  }}
                >
                  {item.status}
                </Text>
              </View>
            </View>
            <Text style={{ fontSize: 16, marginBottom: 10 }}>
              {item.quantity} {item.quantity > 1 ? "items" : "item"}
            </Text>
          </View>
        }
        ListFooterComponent={
          <View>
            <Text style={{ fontSize: 16, marginBottom: 10 }}>
              Order information
            </Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 10,
              }}
            >
              <Text style={{ color: "gray", fontSize: 16 }}>
                Shipping Address:{" "}
              </Text>
              <View style={{ alignItems: "flex-end" }}>
                <Text style={{ fontSize: 16 }}>{item.address},</Text>
                <Text style={{ fontSize: 16 }}>{item.state}</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 10,
              }}
            >
              <View>
                <Text style={{ color: "gray", fontSize: 16, marginBottom: 10 }}>
                  Payment method:{" "}
                </Text>
                <Text style={{ color: "gray", fontSize: 16, marginBottom: 10 }}>
                  Delivery method:{" "}
                </Text>
                <Text style={{ color: "gray", fontSize: 16, marginBottom: 10 }}>
                  Discout:{" "}
                </Text>
                <Text style={{ color: "gray", fontSize: 16, marginBottom: 10 }}>
                  Total amount:{" "}
                </Text>
              </View>
              <View>
                <Text style={{ fontSize: 16, marginBottom: 10 }}>
                  {item.payMeth}
                </Text>
                <Text style={{ fontSize: 16, marginBottom: 10 }}>
                  {item.deliMeth}
                </Text>
                <Text style={{ fontSize: 16, marginBottom: 10 }}>Null</Text>
                <Text style={{ fontSize: 16, marginBottom: 10 }}>
                  ${item.sum}
                </Text>
              </View>
            </View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <TouchableOpacity>
                <View style={[styles.card]}>
                  <Text style={{ fontSize: 16 }}>Reorder</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View
                  style={[
                    styles.card,
                    { backgroundColor: "red", borderColor: "red" },
                  ]}
                >
                  <Text style={{ color: "white", fontSize: 16 }}>
                    Leave feedback
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        }
        data={item.foodIn4}
        renderItem={({ item }) => <OrderDetail item={item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    borderWidth: 1,
    borderRadius: 15,
    width: width / 2.5,
    alignItems: "center",
    height: width / 10,
    justifyContent: "center",
  },
});
