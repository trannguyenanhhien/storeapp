import axios from "axios";
import React, { useState, useEffect } from "react";
import { View, Text, FlatList } from "react-native";
import OrderItem from "../../components/OrderItem";
import { useSelector } from "react-redux";
import { selectOrder } from "../../feature/orderSlice";

export default function Delivered(props) {
  const order = useSelector(selectOrder);
  const { navigation } = props;
  return (
    <View>
      <FlatList
        data={order}
        renderItem={({ item }) => (
          <OrderItem
            info={item}
            onShowDetailClick={() =>
              navigation.navigate("OrderDetails", {
                item,
                name: "Order Details",
              })
            }
          />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}
