import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import axios from "axios";
import TabNavigator from "./TabNavigator";
import { View, Text, ActivityIndicator, StyleSheet } from "react-native";
import { addToBanner, addToCategories, addToFood } from "./feature/getAPI";

export default function Index() {
  const dispatch = useDispatch();
  const [loader, setLoader] = useState(false);
  const getAPI = async () => {
    await axios.get("/banner").then((res) => dispatch(addToBanner(res.data)));
    await axios
      .get("/categories")
      .then((res) => dispatch(addToCategories(res.data)));
    await axios.get("/food").then((res) => dispatch(addToFood(res.data)));
    setTimeout(() => {
      setLoader(true);
    }, 2000);
  };
  useEffect(() => {
    getAPI();
  });

  return (
    <>
      {loader ? (
        <TabNavigator />
      ) : (
        <View style={[styles.container]}>
          <ActivityIndicator size="large" color="green" />
          <Text
            style={{
              textAlign: "center",
              fontSize: 20,
              margin: 30,
              color: "green",
            }}
          >
            FOOD APP
          </Text>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
});
