import { configureStore } from "@reduxjs/toolkit";
import cartSlice from "../feature/cartSlice";
import orderSlice from "../feature/orderSlice";
import getAPI from "../feature/getAPI";

export const store = configureStore({
  reducer: {
    cart: cartSlice,
    order: orderSlice,
    api: getAPI,
  },
});
