import { createSlice } from "@reduxjs/toolkit";

export const orderSlice = createSlice({
  name: "order",
  initialState: {
    order: [
      {
        address: "3 Newbridge Court",
        date: "26-1-2022",
        deliMeth: "GHN",
        foodIn4: [
          {
            categorie: 1,
            count: 1,
            id: 1,
            image: "http://tutofox.com/foodapp//food/american/beef-grill.png",
            name: "Beef Grill",
            price: 24,
          },
        ],
        id: 1,
        name: "Jane Doe",
        payMeth: "COD",
        quantity: 1,
        state: "Chino Hills, CA 91709, U.S",
        status: "Delivered",
        sum: "28.80",
      },
    ],
  },
  reducers: {
    addToOrder: (state, action) => {
      //   let index = state.order.findIndex(
      //     (product) => product.id === action.payload.id
      //   );
      //   index >= 0
      //     ? (state.cart[index].count = state.cart[index].count + 1)
      //     : state.cart.push({
      //         ...action.payload,
      //         count: 1,
      //       });
      state.order.push({
        ...action.payload,
        id: state.order.length + 1,
      });
    },
    // removeToCart: (state, action) => {
    //   let index = state.cart.findIndex(
    //     (product) => product.id === action.payload.id
    //   );
    //   state.cart[index].count > 1
    //     ? (state.cart[index].count = state.cart[index].count - 1)
    //     : state.cart.splice(index, 1);
    // },
  },
});

export const { addToOrder } = orderSlice.actions;
export const selectOrder = (state) => state.order.order;
export default orderSlice.reducer;
