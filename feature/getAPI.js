import { createSlice } from "@reduxjs/toolkit";

export const getAPI = createSlice({
  name: "api",
  initialState: {
    banner: [],
    categories: [],
    food: [],
  },
  reducers: {
    addToBanner: (state, action) => {
      state.banner = [...action.payload];
    },
    addToCategories: (state, action) => {
      state.categories = [...action.payload];
    },
    addToFood: (state, action) => {
      state.food = [...action.payload];
    },
  },
});

export const { addToBanner, addToCategories, addToFood } = getAPI.actions;
export const selectAPI = (state) => state.api;
export default getAPI.reducer;
