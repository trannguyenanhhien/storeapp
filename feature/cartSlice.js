import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cart: [],
  },
  reducers: {
    addToCart: (state, action) => {
      let index = state.cart.findIndex(
        (product) => product.id === action.payload.id
      );
      index >= 0
        ? (state.cart[index].count = state.cart[index].count + 1)
        : state.cart.push({
            ...action.payload,
            count: 1,
          });
    },
    removeToCart: (state, action) => {
      let index = state.cart.findIndex(
        (product) => product.id === action.payload.id
      );
      state.cart[index].count > 1
        ? (state.cart[index].count = state.cart[index].count - 1)
        : state.cart.splice(index, 1);
    },
    deleteToCart: (state) => {
      let length = state.cart.length;
      state.cart.splice(0, length);
    },
  },
});

export const { addToCart, removeToCart, deleteToCart } = cartSlice.actions;
export const selectCart = (state) => state.cart.cart;
export default cartSlice.reducer;
