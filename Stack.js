import * as React from "react";
import { Text, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Categories from "./screens/Categories";
import Cart from "./screens/Cart";
import Profile from "./screens/Profile";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useSelector } from "react-redux";
import { selectCart } from "./feature/cartSlice";
import { selectOrder } from "./feature/orderSlice";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import OrderDetails from "./screens/orders/OrderDetails";
import DeliveredStack from "./NormalStack";

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const OrderStack = function () {
  return (
    <>
      <Stack.Navigator
        screenOptions={{
          headerTitleAlign: "center",
        }}
      >
        <Stack.Screen
          name="DeliveredStack"
          component={DeliveredStack}
          options={{ title: "Orders" }}
        />
        <Stack.Screen
          name="OrderDetails"
          component={OrderDetails}
          options={({ route }) => ({ title: route.params.name })}
        />
      </Stack.Navigator>
    </>
  );
};

const CartTabs = function () {
  const cart = useSelector(selectCart);
  const order = useSelector(selectOrder);
  return (
    <>
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarActiveTintColor: "red",
          tabBarInactiveTintColor: "#B5B5B5",
        }}
        initialRouteName="Home"
      >
        <Tab.Screen
          name="Home"
          component={Categories}
          options={{
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons
                name="home-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
        <Tab.Screen
          name="CartStack"
          component={Cart}
          options={{
            tabBarLabel: "Cart",
            tabBarIcon: ({ color }) => {
              if (cart.length > 0) {
                return (
                  <>
                    <MaterialCommunityIcons
                      name="cart-outline"
                      color={color}
                      size={26}
                    />
                    <View
                      style={{
                        width: 25,
                        height: 25,
                        position: "absolute",
                        borderRadius: 200 / 2,
                        alignItems: "center",
                        right: 22,
                        top: -6,
                        backgroundColor: "#33c37d",
                        borderColor: "white",
                        borderWidth: 2,
                      }}
                    >
                      <Text
                        style={{
                          fontWeight: "bold",
                          fontSize: 15,
                          color: "white",
                        }}
                      >
                        {cart.length}
                      </Text>
                    </View>
                  </>
                );
              } else {
                return (
                  <MaterialCommunityIcons
                    name="cart-outline"
                    color={color}
                    size={26}
                  />
                );
              }
            },
          }}
        />
        <Tab.Screen
          name="Orders"
          component={OrderStack}
          options={{
            tabBarIcon: ({ color }) => {
              if (order.length > 0) {
                return (
                  <>
                    <MaterialCommunityIcons
                      name="truck-outline"
                      color={color}
                      size={26}
                    />
                    <View
                      style={{
                        width: 25,
                        height: 25,
                        position: "absolute",
                        borderRadius: 200 / 2,
                        alignItems: "center",
                        right: 22,
                        top: -6,
                        backgroundColor: "#33c37d",
                        borderColor: "white",
                        borderWidth: 2,
                      }}
                    >
                      <Text
                        style={{
                          fontWeight: "bold",
                          fontSize: 15,
                          color: "white",
                        }}
                      >
                        {order.length}
                      </Text>
                    </View>
                  </>
                );
              } else {
                return (
                  <MaterialCommunityIcons
                    name="truck-outline"
                    color={color}
                    size={26}
                  />
                );
              }
            },
            // headerShown: true,
            headerTitleAlign: "center",
          }}
        />
        <Tab.Screen
          name="Profile"
          component={Profile}
          options={{
            tabBarLabel: "Profile",
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons
                name="account-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default CartTabs;
