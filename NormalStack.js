import * as React from "react";
import { Dimensions } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Delivered from "./screens/orders/Delivered";
import Cancelled from "./screens/orders/Cancelled";
import Processing from "./screens/orders/Processing";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const TabTop = createMaterialTopTabNavigator();
var { height, width } = Dimensions.get("window");

const DeliveredStack = function () {
  return (
    <>
      <TabTop.Navigator
        screenOptions={{
          tabBarInactiveTintColor: "black",
          tabBarIndicatorStyle: {
            backgroundColor: "black",
            height: height / 22,
            borderRadius: 20,
          },
          tabBarIndicatorContainerStyle: { marginBottom: height / 180 },
          tabBarShowIcon: false,
          tabBarActiveTintColor: "white",
          tabBarStyle: {
            margin: 10,
            backgroundColor: "transparent",
            elevation: 0,
          },
          tabBarPressColor: "transparent",
          tabBarLabelStyle: { textTransform: "capitalize", fontSize: 15 },
        }}
      >
        <TabTop.Screen name="Delivered" component={Delivered} />
        <TabTop.Screen name="Processing" component={Processing} />
        <TabTop.Screen name="Cancelled" component={Cancelled} />
      </TabTop.Navigator>
    </>
  );
};

export default DeliveredStack;
