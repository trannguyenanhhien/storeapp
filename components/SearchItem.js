import React from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
var { height, width } = Dimensions.get("window");
export default function SearchItem(props) {
  const { item, onClick } = props;
  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        margin: 10,
        marginHorizontal: 30,
        justifyContent: "space-between",
      }}
    >
      <Image
        style={{ height: width / 10, width: width / 10 }}
        source={{ uri: item.image }}
      />
      <View style={{ marginHorizontal: 20 }}>
        <Text>{item.name}</Text>
        <Text>
          {item.categorie == 1
            ? "American"
            : item.categorie == 2
            ? "Burger"
            : item.categorie == 3
            ? "Pizza"
            : "Drink"}
        </Text>
      </View>
      <Text>${item.price}</Text>
      <TouchableOpacity onPress={onClick}>
        <Text
          style={{
            backgroundColor: "red",
            padding: 10,
            borderRadius: 20,
            color: "white",
          }}
        >
          Add to cart
        </Text>
      </TouchableOpacity>
    </View>
  );
}
