import React from "react";
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default function ProfileItem(props) {
  const { info, onClick } = props;
  return (
    <TouchableOpacity onPress={onClick}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          marginBottom: 20,
        }}
      >
        <View>
          <Text style={{ fontSize: 16, fontWeight:"bold" }}>{info.name}</Text>
          <Text style={{ color: "gray" }}>{info.decr}</Text>
        </View>
        <MaterialCommunityIcons name="chevron-right" size={26} color={"gray"} />
      </View>
    </TouchableOpacity>
  );
}
