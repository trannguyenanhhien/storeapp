import React from "react";
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { RadioButton } from "react-native-paper";
var { height, width } = Dimensions.get("window");
export default function SAItem(props) {
  const { info } = props;
  const [checked, setChecked] = React.useState("true");
  return (
    <View style={styles.boxIn4}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingBottom: 5,
        }}
      >
        <Text style={{ fontSize: 17, fontWeight: "bold" }}>{info.name}</Text>
        <TouchableOpacity>
          <Text style={styles.edit}>{info.edit ? "Edit" : null}</Text>
        </TouchableOpacity>
      </View>
      <Text style={{ fontSize: 17 }}>{info.address}</Text>
      <Text style={{ fontSize: 17 }}>{info.state}</Text>
      {info.edit ? (
        <View
          style={{
            flexDirection: "row",
            paddingBottom: 5,
            alignItems: "center",
          }}
        >
          <RadioButton
            value="first"
            color="black"
            status={checked === "false" ? "checked" : "unchecked"}
            onPress={() =>
              checked === "true" ? setChecked("false") : setChecked("true")
            }
          />
          <Text>Use as the shipping address</Text>
        </View>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  boxIn4: {
    marginVertical: width / 20 - 10,
    backgroundColor: "white",
    borderRadius: 15,
    padding: 20,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    elevation: 3,
  },
  edit: {
    fontWeight: "bold",
    fontSize: 18,
    color: "red",
  },
});
