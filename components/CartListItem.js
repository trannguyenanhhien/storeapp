import React from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
var { height, width } = Dimensions.get("window");
export default function ProductListItem(props) {
  const { food, onAddToCartClick, onRemoveToCartClick } = props;
  return (
    <View style={styles.card}>
      <View style={{ height: 20 }} />
      <View style={styles.container}>
        <View style={styles.row}>
          <Image style={styles.img} source={{ uri: food.image }} />
          <View style={styles.cardIn4}>
            <View>
              <Text style={styles.cardProduct}>{food.name}</Text>
              <Text>Description</Text>
            </View>
            <Text style={{ flex: 1 }} />
            <View style={styles.priceRow}>
              <Text style={styles.price}>${food.price * food.count}</Text>
              <View style={styles.actions}>
                <TouchableOpacity onPress={onAddToCartClick}>
                  <MaterialCommunityIcons
                    style={{elevation:5, borderRadius:50}}
                    name="plus-circle"
                    color={"white"}
                    size={32}
                  />
                </TouchableOpacity>
                <Text style={styles.count}>{food.count}</Text>
                <TouchableOpacity onPress={onRemoveToCartClick}>
                  <MaterialCommunityIcons
                    style={{elevation:5, borderRadius:50}}
                    name="minus-circle"
                    color={"white"}
                    size={32}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    borderRadius: 10,
    marginVertical: 10,
    elevation: 3,
    paddingBottom: 5,
  },
  container: {
    backgroundColor: "transparent",
    flex: 1,
  },
  row: {
    margin: 5,
    backgroundColor: "transparent",
    flexDirection: "row",
    borderColor: "#cccccc",
    paddingBottom: 5,
  },
  img: {
    width: width / 3.5,
    height: width / 3.5,
    marginRight: 15,
  },
  cardIn4: {
    backgroundColor: "transparent",
    flex: 1,
    justifyContent: "space-between",
  },
  cardProduct: {
    fontSize: 20,
    fontWeight: "bold",
  },
  priceRow: {
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  price: {
    fontWeight: "bold",
    color: "red",
    fontSize: 22,
  },
  actions: {
    flexDirection: "row",
  },
  count: {
    fontWeight: "bold",
    paddingHorizontal: 8,
    fontSize: 18,
  },
});
