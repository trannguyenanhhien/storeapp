import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";

export default function CategoryListItem(props) {
  const { category, onPress } = props;
  return (
    <View>
      <TouchableOpacity
        style={[styles.container, { backgroundColor: category.color }]}
        activeOpacity={0.5}
        onPress={onPress}
      >
        <Image style={styles.categoryImage} source={{ uri: category.image }} />
        <Text style={styles.categoryName}>{category.name}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "red",
    margin: 5,
    alignItems: "center",
    borderRadius: 10,
    padding: 10,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    //Android
    elevation: 3,
  },
  categoryImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
  },
  categoryName: {
    fontWeight: "bold",
    fontSize: 22,
  },
  title: {
    textTransform: "uppercase",
    marginBottom: 8,
    fontWeight: "700",
  },
});
