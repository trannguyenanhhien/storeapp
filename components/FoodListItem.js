import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

var { height, width } = Dimensions.get("window");
export default function FoodListItem(props) {
  const { food, onAddToCartClick } = props;
  return (
    <View>
      <View style={styles.container}>
        <Image
          style={styles.foodImage}
          source={{ uri: food.image }}
          resizeMode="contain"
        />
        <View
          style={{
            height: width / 2 - 20 - 90,
            width: width / 2 - 20 - 10,
            backgroundColor: "transparent",
          }}
        />
        <Text style={styles.foodName}>{food.name}</Text>
        <Text>Description Food</Text>
        <Text style={{ fontSize: 20, color: "red" }}>${food.price}</Text>
        <TouchableOpacity style={styles.addCart} onPress={onAddToCartClick}>
          <Text style={styles.cartText}>Add Cart</Text>
          <MaterialCommunityIcons
            name="plus-circle"
            color={"white"}
            size={25}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: width / 2 - 20,
    padding: 10,
    borderRadius: 10,
    marginTop: 55,
    marginBottom: 5,
    marginLeft: 10,
    alignItems: "center",
    elevation: 3,
    shadowOpacity: 0.05,
    backgroundColor: "white",
  },
  foodImage: {
    width: width / 2 - 20 - 10,
    height: width / 2 - 20 - 30,
    backgroundColor: "transparent",
    position: "absolute",
    top: -45,
  },
  foodName: {
    fontSize: width / 22,
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 5,
  },
  addCart: {
    width: width / 2 - 40,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    padding: 5,
    flexDirection: "row",
  },
  cartText: {
    fontSize: 18,
    color: "white",
    fontWeight: "bold",
    marginRight: 6,
  },
});
