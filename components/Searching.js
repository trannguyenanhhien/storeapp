import React from "react";
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

var { height, width } = Dimensions.get("window");
function Searching(props) {
  const { searchClick } = props;
  return (
    <TouchableOpacity onPress={searchClick}>
      <View style={{ marginTop: 30 }}>
        <MaterialCommunityIcons name="magnify" size={26} />
      </View>
    </TouchableOpacity>
  );
}

function SearchActive(props) {
  const { searchChange, cancelClick } = props;
  return (
    <View style={{ flexDirection: "row", alignItems: "center" }}>
      <View style={{ marginLeft: 10 }}>
        <TextInput
          style={{
            height: 45,
            width: width / 1.3,
            borderWidth: 1,
            marginTop: 40,
            paddingHorizontal: 10,
            marginBottom: 5,
          }}
          onChangeText={searchChange}
          placeholder="Text something you want to search here"
        />
      </View>
      <TouchableOpacity
        style={{ marginTop: 40, marginRight: 10 }}
        onPress={cancelClick}
      >
        <Text
          style={{
            color: "blue",
            fontWeight: "bold",
            fontSize: 17,
            marginBottom: 5,
            marginLeft: 10,
          }}
        >
          Cancel
        </Text>
      </TouchableOpacity>
    </View>
  );
}

export { Searching, SearchActive };
