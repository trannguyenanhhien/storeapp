import React from "react";
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { RadioButton } from "react-native-paper";
var { height, width } = Dimensions.get("window");
export default function OrderItem(props) {
  const { info, onShowDetailClick } = props;
  return (
    <View style={styles.boxIn4}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingBottom: 5,
        }}
      >
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>
          Order No{info.id}
        </Text>
        <Text style={styles.edit}>{info.date}</Text>
      </View>
      <View style={{ height: height / 100 }} />
      <View
        style={{
          flexDirection: "row",
          paddingBottom: 5,
        }}
      >
        <Text style={styles.textTitle}>Tracking number: </Text>
        <Text style={styles.textIn4}>IW{info.id + 12584}</Text>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingBottom: 5,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.textTitle}>Quantity: </Text>
          <Text style={styles.textIn4}>{info.quantity}</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.textTitle}>Total Amount: </Text>
          <Text style={styles.textIn4}>${info.sum}</Text>
        </View>
      </View>
      <View style={{ height: height / 100 }} />
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <TouchableOpacity onPress={onShowDetailClick}>
          <View style={{ borderWidth: 1, padding: 10, borderRadius: 15 }}>
            <Text styles={styles.textIn4}>Details</Text>
          </View>
        </TouchableOpacity>
        <Text
          style={{
            color:
              info.status === "Delivered"
                ? "green"
                : info.status === "Processing"
                ? "orange"
                : "red",
          }}
        >
          {info.status}
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  boxIn4: {
    marginVertical: width / 20 - 10,
    backgroundColor: "white",
    borderRadius: 15,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    elevation: 3,
    margin: width / 20,
    padding: width / 20,
  },
  edit: {
    color: "gray",
    fontStyle: "italic",
  },
  textIn4: {
    fontWeight: "bold",
    fontSize: 16,
  },
  textTitle: {
    fontSize: 16,
    color: "gray",
  },
});
