import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";

var { height, width } = Dimensions.get("window");
export default function OrderDetail(props) {
  const { item } = props;
  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.foodImage}
          source={{
            uri: item.image,
          }}
          resizeMode="contain"
        />
      </View>
      <View style={{ flex: 1, padding: 10 }}>
        <Text style={{ fontSize: 18, fontWeight: "bold", flex: 0.22 }}>
          {item.name}
        </Text>
        <Text style={{ flex: 0.22, color: "gray" }}>
          {item.categorie == 1
            ? "American"
            : item.categorie == 2
            ? "Burger"
            : item.categorie == 3
            ? "Pizza"
            : "Drink"}
        </Text>
        <Text style={{ flex: 0.22, color: "gray" }}>Description Food</Text>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            flex: 0.3,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "gray" }}>Units: </Text>
            <Text>{item.count}</Text>
          </View>
          <View>
            <Text style={{ fontSize: 20, color: "black" }}>${item.price}</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 2,
    marginVertical: 10,
    borderRadius: 10,
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 50,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
  },
  foodImage: {
    width: width / 3,
    height: width / 3,
    backgroundColor: "transparent",
    marginHorizontal: 10,
  },
});
