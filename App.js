import React from "react";
import axios from "axios";
import TabNavigator from "./TabNavigator";
import { Provider } from "react-redux";
import { store } from "./app/store";
import IndexApp from "./IndexApp";

axios.defaults.baseURL = "https://demomockdb.herokuapp.com";

export default function App() {
  return (
    <Provider store={store}>
      <IndexApp />
    </Provider>
  );
}
