<h1>This is my React Native application DEMO</h1>

<h1>Run project in development</h1>
<ul>
    <li>Setting up the development environment: https://reactnative.dev/docs/environment-setup.</li>
    <li>Install dependencies: yarn install (or npm install).</li>
    <li>Run on Android: yarn android (or npm run android).</li>
    <li>Run on iOS: yarn ios (or npm run ios).</li>
    <li>Run on both Android & iOS: yarn mobile (or npm run mobile).</li>
</ul>

<h1>Preview</h1>
Using Expo Client App (Expo Go) and scan this QR code:
<br>
<br>
<p>
    <img src="https://i.pinimg.com/564x/2b/df/a8/2bdfa8a6cbb062b9bca31d3430158201.jpg" width="250" title="hover text">
</p>

<h3>Overview</h3>
<img src="assets/Overview.gif">
<br>
<h3>Signup</h3>
<img src="assets/Signup.gif">
<br>
<h3>Login & Logout</h3>
<img src="assets/Login&Logout.gif">
<br>
<h3>Checkout</h3>
<img src="assets/Checkout.gif">
<br>
<h3>Orders</h3>
<img src="assets/Orders.gif">