import * as React from "react";
import Checkout from "./screens/checkout/Checkout";
import CartTabs from "./Stack";
import ShippingAddresses from "./screens/checkout/ShippingAddresses";
import PaymentMethod from "./screens/checkout/PaymentMethods";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import Settings from "./screens/profile/Settings";
import Account from "./screens/profile/Account";
import Login from "./screens/auth/Login"
import Signup from "./screens/auth/Signup"
const Stack = createNativeStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Cart"
          component={CartTabs}
          options={{
            title: "Cart",
            headerTitleAlign: "center",
            headerMode: "none",
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Checkout"
          component={Checkout}
          options={{
            title: "Checkout",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Settings"
          component={Settings}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Account"
          component={Account}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ShippingAddresses"
          component={ShippingAddresses}
          options={{
            title: "Shipping Addresses",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="PaymentMethod"
          component={PaymentMethod}
          options={{
            title: "Payment Method",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default function TabNavigator() {
  return <MyStack />;
}
