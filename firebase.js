// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
import * as firebase from "firebase";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA_sLJtbeCOhoR9EXRsYEfI_AGNRt66M4w",
  authDomain: "fir-auth-5accb.firebaseapp.com",
  projectId: "fir-auth-5accb",
  storageBucket: "fir-auth-5accb.appspot.com",
  messagingSenderId: "988333780048",
  appId: "1:988333780048:web:26e2703864fe9c5475b555",
};

// Initialize Firebase
let app;
if (firebase.apps.length === 0) {
  app = firebase.initializeApp(firebaseConfig);
} else {
  app = firebase.app();
}
const auth = firebase.auth();
export { auth };
